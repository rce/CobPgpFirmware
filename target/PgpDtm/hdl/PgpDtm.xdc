##############################################################################
## This file is part of 'Example RCE Project'.
## It is subject to the license terms in the LICENSE.txt file found in the 
## top-level directory of this distribution and at: 
##    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
## No part of 'Example RCE Project', including this file, 
## may be copied, modified, propagated, or distributed except according to 
## the terms contained in the LICENSE.txt file.
##############################################################################
create_clock -name locRefClkP -period  4.000 [get_ports {locRefClkP}]

create_generated_clock  -name stableClk [get_pins {U_AppCore/PgpCore_Inst/IBUFDS_GTE2_Inst/ODIV2}] 
create_generated_clock  -name pgpClk    [get_pins {U_AppCore/PgpCore_Inst/U_ClkManager/MmcmGen.U_Mmcm/CLKOUT0}] 

set_clock_groups -asynchronous -group [get_clocks {pgpClk}] -group [get_clocks {stableClk}]
set_clock_groups -asynchronous -group [get_clocks {pgpClk}] -group [get_clocks {sysClk125}]
set_clock_groups -asynchronous -group [get_clocks {pgpClk}] -group [get_clocks {sysClk200}]