-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpDtm.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2015-07-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.RceG3Pkg.all;

entity PgpDtm is
   port (
      -- Debug
      led         : out   slv(1 downto 0);
      -- I2C
      i2cSda      : inout sl;
      i2cScl      : inout sl;
      -- PCI Express
      pciRefClkP  : in    sl;
      pciRefClkM  : in    sl;
      pciRxP      : in    sl;
      pciRxM      : in    sl;
      pciTxP      : out   sl;
      pciTxM      : out   sl;
      pciResetL   : out   sl;
      -- COB Ethernet
      ethRxP      : in    sl;
      ethRxM      : in    sl;
      ethTxP      : out   sl;
      ethTxM      : out   sl;
      -- Reference Clock
      locRefClkP  : in    sl;
      locRefClkM  : in    sl;
      -- Clock Select
      clkSelA     : out   sl;
      clkSelB     : out   sl;
      -- Base Ethernet
      ethRxCtrl   : in    slv(1 downto 0);
      ethRxClk    : in    slv(1 downto 0);
      ethRxDataA  : in    Slv(1 downto 0);
      ethRxDataB  : in    Slv(1 downto 0);
      ethRxDataC  : in    Slv(1 downto 0);
      ethRxDataD  : in    Slv(1 downto 0);
      ethTxCtrl   : out   slv(1 downto 0);
      ethTxClk    : out   slv(1 downto 0);
      ethTxDataA  : out   Slv(1 downto 0);
      ethTxDataB  : out   Slv(1 downto 0);
      ethTxDataC  : out   Slv(1 downto 0);
      ethTxDataD  : out   Slv(1 downto 0);
      ethMdc      : out   Slv(1 downto 0);
      ethMio      : inout Slv(1 downto 0);
      ethResetL   : out   Slv(1 downto 0);
      -- RTM High Speed
      dtmToRtmHsP : out   sl;
      dtmToRtmHsM : out   sl;
      rtmToDtmHsP : in    sl;
      rtmToDtmHsM : in    sl;
      -- -- RTM Low Speed
      -- dtmToRtmLsP : inout slv(5 downto 0);
      -- dtmToRtmLsM : inout slv(5 downto 0);
      -- -- DPM Signals
      -- dpmClkP     : out   slv(2 downto 0);
      -- dpmClkM     : out   slv(2 downto 0);
      -- dpmFbP      : in    slv(7 downto 0);
      -- dpmFbM      : in    slv(7 downto 0);
      -- Backplane Clocks
      bpClkIn     : in    slv(5 downto 0);
      bpClkOut    : out   slv(5 downto 0);
      -- -- Spare Signals
      --plSpareP     : inout slv(4 downto 0);
      --plSpareM     : inout slv(4 downto 0);
      -- IPMI
      dtmToIpmiP  : out   slv(1 downto 0);
      dtmToIpmiM  : out   slv(1 downto 0));
end PgpDtm;

architecture TOP_LEVEL of PgpDtm is

   constant TPD_C : time := 1 ns;

   signal axiClk    : sl;
   signal axiClkRst : sl;

   signal sysClk125    : sl;
   signal sysClk125Rst : sl;

   signal sysClk200    : sl;
   signal sysClk200Rst : sl;

   signal extAxilReadMaster  : AxiLiteReadMasterType;
   signal extAxilReadSlave   : AxiLiteReadSlaveType;
   signal extAxilWriteMaster : AxiLiteWriteMasterType;
   signal extAxilWriteSlave  : AxiLiteWriteSlaveType;

   signal dmaClk    : slv(2 downto 0);
   signal dmaClkRst : slv(2 downto 0);

   signal dmaObMaster : AxiStreamMasterArray(2 downto 0);
   signal dmaObSlave  : AxiStreamSlaveArray(2 downto 0);
   signal dmaIbMaster : AxiStreamMasterArray(2 downto 0);
   signal dmaIbSlave  : AxiStreamSlaveArray(2 downto 0);

   attribute KEEP_HIERARCHY              : string;
   attribute KEEP_HIERARCHY of U_DtmCore : label is "TRUE";
   attribute KEEP_HIERARCHY of U_AppCore : label is "TRUE";
   
begin

   --------------------------------------------------
   -- Core
   --------------------------------------------------
   U_DtmCore : entity work.DtmCore
      generic map (
         TPD_G          => TPD_C,
         RCE_DMA_MODE_G => RCE_DMA_AXIS_C,
         OLD_BSI_MODE_G => false,
         HSIO_MODE_G    => false) 
      port map (
         -- I2C
         i2cSda             => i2cSda,
         i2cScl             => i2cScl,
         -- PCI Exress, Unused when HSIO_MODE_G = true
         pciRefClkP         => pciRefClkP,
         pciRefClkM         => pciRefClkM,
         pciRxP             => pciRxP,
         pciRxM             => pciRxM,
         pciTxP             => pciTxP,
         pciTxM             => pciTxM,
         pciResetL          => pciResetL,
         -- COB Ethernet, Unused when HSIO_MODE_G = true
         ethRxP             => ethRxP,
         ethRxM             => ethRxM,
         ethTxP             => ethTxP,
         ethTxM             => ethTxM,
         -- Clock Select
         clkSelA            => clkSelA,
         clkSelB            => clkSelB,
         -- Base Ethernet
         ethRxCtrl          => ethRxCtrl,
         ethRxClk           => ethRxClk,
         ethRxDataA         => ethRxDataA,
         ethRxDataB         => ethRxDataB,
         ethRxDataC         => ethRxDataC,
         ethRxDataD         => ethRxDataD,
         ethTxCtrl          => ethTxCtrl,
         ethTxClk           => ethTxClk,
         ethTxDataA         => ethTxDataA,
         ethTxDataB         => ethTxDataB,
         ethTxDataC         => ethTxDataC,
         ethTxDataD         => ethTxDataD,
         ethMdc             => ethMdc,
         ethMio             => ethMio,
         ethResetL          => ethResetL,
         -- IPMI
         dtmToIpmiP         => dtmToIpmiP,
         dtmToIpmiM         => dtmToIpmiM,
         -- Clocks
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => sysClk200,
         sysClk200Rst       => sysClk200Rst,
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk             => axiClk,
         axiClkRst          => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         -- DMA Interfaces
         dmaClk             => dmaClk,
         dmaClkRst          => dmaClkRst,
         dmaState           => open,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave,
         -- User Interrupts         
         userInterrupt      => (others => '0'));

   --------------------------------------------------
   -- Application Core Module
   --------------------------------------------------
   U_AppCore : entity work.PgpDtmAppCore
      generic map (
         TPD_G => TPD_C)
      port map (
         -- Debug
         led                => led,
         -- 250 MHz Reference Oscillator 
         locRefClkP         => locRefClkP,
         locRefClkM         => locRefClkM,
         -- RTM High Speed
         dtmToRtmHsP        => dtmToRtmHsP,
         dtmToRtmHsM        => dtmToRtmHsM,
         rtmToDtmHsP        => rtmToDtmHsP,
         rtmToDtmHsM        => rtmToDtmHsM,
         -- -- RTM Low Speed
         -- dtmToRtmLsP        => dtmToRtmLsP,
         -- dtmToRtmLsM        => dtmToRtmLsM,
         -- -- DPM Signals
         -- dpmClkP            => dpmClkP,
         -- dpmClkM            => dpmClkM,
         -- dpmFbP             => dpmFbP,
         -- dpmFbM             => dpmFbM,
         -- Backplane Clocks
         bpClkIn            => bpClkIn,
         bpClkOut           => bpClkOut,
         -- CPU System Clocks
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => sysClk200,
         sysClk200Rst       => sysClk200Rst,
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk             => axiClk,
         axiRst             => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         -- DMA Interfaces
         dmaClk             => dmaClk,
         dmaRst             => dmaClkRst,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave);        

end architecture TOP_LEVEL;
