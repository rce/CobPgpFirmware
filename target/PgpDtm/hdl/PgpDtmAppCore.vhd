-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpDtmAppCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2015-07-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.AxiLitePkg.all;
use work.RceG3Pkg.all;

library unisim;
use unisim.vcomponents.all;

entity PgpDtmAppCore is
   generic (
      TPD_G : time := 1 ns);
   port (
      -- Debug
      led                : out slv(1 downto 0);
      -- 250 MHz Reference Oscillator 
      locRefClkP         : in  sl;
      locRefClkM         : in  sl;
      -- RTM High Speed
      dtmToRtmHsP        : out sl;
      dtmToRtmHsM        : out sl;
      rtmToDtmHsP        : in  sl;
      rtmToDtmHsM        : in  sl;
      -- -- RTM Low Speed
      -- dtmToRtmLsP        : inout slv(5 downto 0);
      -- dtmToRtmLsM        : inout slv(5 downto 0);
      -- -- DPM Signals
      -- dpmClkP            : out   slv(2 downto 0);
      -- dpmClkM            : out   slv(2 downto 0);
      -- dpmFbP             : in    slv(7 downto 0);
      -- dpmFbM             : in    slv(7 downto 0);
      -- Backplane Clocks
      bpClkIn            : in  slv(5 downto 0);
      bpClkOut           : out slv(5 downto 0);
      -- CPU System Clocks
      sysClk125          : in  sl;
      sysClk125Rst       : in  sl;
      sysClk200          : in  sl;
      sysClk200Rst       : in  sl;
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk             : in  sl;
      axiRst             : in  sl;
      extAxilReadMaster  : in  AxiLiteReadMasterType;
      extAxilReadSlave   : out AxiLiteReadSlaveType;
      extAxilWriteMaster : in  AxiLiteWriteMasterType;
      extAxilWriteSlave  : out AxiLiteWriteSlaveType;
      -- DMA Interfaces
      dmaClk             : out slv(2 downto 0);
      dmaRst             : out slv(2 downto 0);
      dmaObMaster        : in  AxiStreamMasterArray(2 downto 0);
      dmaObSlave         : out AxiStreamSlaveArray(2 downto 0);
      dmaIbMaster        : out AxiStreamMasterArray(2 downto 0);
      dmaIbSlave         : in  AxiStreamSlaveArray(2 downto 0));         
end PgpDtmAppCore;

architecture mapping of PgpDtmAppCore is

   constant NUM_AXI_MASTERS_C : natural := 1;
   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      0               => (
         baseAddr     => X"A0000000",
         addrBits     => 12,
         connectivity => X"0001"));    
   signal axiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

begin

   --------------------
   -- AXI-Lite Crossbar
   --------------------
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         TPD_G              => TPD_G,
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         DEC_ERROR_RESP_G   => AXI_RESP_OK_C,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         axiClk              => axiClk,
         axiClkRst           => axiRst,
         sAxiWriteMasters(0) => extAxilWriteMaster,
         sAxiWriteSlaves(0)  => extAxilWriteSlave,
         sAxiReadMasters(0)  => extAxilReadMaster,
         sAxiReadSlaves(0)   => extAxilReadSlave,
         mAxiWriteMasters    => axiWriteMasters,
         mAxiWriteSlaves     => axiWriteSlaves,
         mAxiReadMasters     => axiReadMasters,
         mAxiReadSlaves      => axiReadSlaves);     

   -------------------
   -- Application Core
   -------------------
   PgpCore_Inst : entity work.PgpCore
      generic map (
         TPD_G       => TPD_G,
         NUM_LANES_G => NUM_AXI_MASTERS_C)
      port map (
         -- 250 MHz Reference Oscillator 
         locRefClkP      => locRefClkP,
         locRefClkM      => locRefClkM,
         -- High Speed IOs
         gtTxP(0)        => dtmToRtmHsP,
         gtTxN(0)        => dtmToRtmHsM,
         gtRxP(0)        => rtmToDtmHsP,
         gtRxN(0)        => rtmToDtmHsM,
         -- DMA Interfaces
         dmaClk          => dmaClk,
         dmaRst          => dmaRst,
         dmaObMaster     => dmaObMaster,
         dmaObSlave      => dmaObSlave,
         dmaIbMaster     => dmaIbMaster,
         dmaIbSlave      => dmaIbSlave,
         -- AXI-Lite Interface
         axiClk          => axiClk,
         axiRst          => axiRst,
         axiReadMasters  => axiReadMasters,
         axiReadSlaves   => axiReadSlaves,
         axiWriteMasters => axiWriteMasters,
         axiWriteSlaves  => axiWriteSlaves);

   -----------------------
   -- Misc. Configurations
   -----------------------  

   led      <= (others => '0');
   bpClkOut <= (others => '0');

end mapping;
