-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpDpm.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2015-07-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.RceG3Pkg.all;

entity PgpDpm is
   port (
      -- Debug
      led         : out   slv(1 downto 0);
      -- I2C
      i2cSda      : inout sl;
      i2cScl      : inout sl;
      -- Ethernet
      ethRxP      : in    slv(0 downto 0);
      ethRxM      : in    slv(0 downto 0);
      ethTxP      : out   slv(0 downto 0);
      ethTxM      : out   slv(0 downto 0);
      ethRefClkP  : in    sl;
      ethRefClkM  : in    sl;
      -- RTM High Speed
      dpmToRtmHsP : out   slv(11 downto 0);
      dpmToRtmHsM : out   slv(11 downto 0);
      rtmToDpmHsP : in    slv(11 downto 0);
      rtmToDpmHsM : in    slv(11 downto 0);
      -- Reference Clocks
      locRefClkP  : in    sl;
      locRefClkM  : in    sl;
      -- DTM Signals
      dtmRefClkP  : in    sl;
      dtmRefClkM  : in    sl;
      dtmClkP     : in    slv(1 downto 0);
      dtmClkM     : in    slv(1 downto 0);
      dtmFbP      : out   sl;
      dtmFbM      : out   sl;
      -- Clock Select
      clkSelA     : out   slv(1 downto 0);
      clkSelB     : out   slv(1 downto 0));
end PgpDpm;

architecture TOP_LEVEL of PgpDpm is

   constant TPD_C : time := 1 ns;

   signal axiClk    : sl;
   signal axiClkRst : sl;

   signal sysClk125    : sl;
   signal sysClk125Rst : sl;

   signal sysClk200    : sl;
   signal sysClk200Rst : sl;

   signal extAxilReadMaster  : AxiLiteReadMasterType;
   signal extAxilReadSlave   : AxiLiteReadSlaveType;
   signal extAxilWriteMaster : AxiLiteWriteMasterType;
   signal extAxilWriteSlave  : AxiLiteWriteSlaveType;

   signal dmaClk    : slv(2 downto 0);
   signal dmaClkRst : slv(2 downto 0);

   signal dmaObMaster : AxiStreamMasterArray(2 downto 0);
   signal dmaObSlave  : AxiStreamSlaveArray(2 downto 0);
   signal dmaIbMaster : AxiStreamMasterArray(2 downto 0);
   signal dmaIbSlave  : AxiStreamSlaveArray(2 downto 0);

   signal iethRxP : slv(3 downto 0);
   signal iethRxM : slv(3 downto 0);
   signal iethTxP : slv(3 downto 0);
   signal iethTxM : slv(3 downto 0);

   attribute KEEP_HIERARCHY              : string;
   attribute KEEP_HIERARCHY of U_DpmCore : label is "TRUE";
   attribute KEEP_HIERARCHY of U_AppCore : label is "TRUE";
   
begin

   --------------------------------------------------
   -- Core
   --------------------------------------------------
   U_DpmCore : entity work.DpmCore
      generic map (
         TPD_G          => TPD_C,
         ETH_10G_EN_G   => false,
         RCE_DMA_MODE_G => RCE_DMA_AXIS_C,
         OLD_BSI_MODE_G => false,
         AXI_ST_COUNT_G => 3) 
      port map (
         -- I2C
         i2cSda             => i2cSda,
         i2cScl             => i2cScl,
         -- Ethernet
         ethRxP             => iethRxP,
         ethRxM             => iethRxM,
         ethTxP             => iethTxP,
         ethTxM             => iethTxM,
         ethRefClkP         => ethRefClkP,
         ethRefClkM         => ethRefClkM,
         -- Clock Select
         clkSelA            => clkSelA,
         clkSelB            => clkSelB,
         -- Clocks
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => sysClk200,
         sysClk200Rst       => sysClk200Rst,
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk             => axiClk,
         axiClkRst          => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         -- DMA Interfaces
         dmaClk             => dmaClk,
         dmaClkRst          => dmaClkRst,
         dmaState           => open,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave,
         -- User Interrupts
         userInterrupt      => (others => '0'));  

   -- 1 GigE Mapping
   ethTxP(0)           <= iethTxP(0);
   ethTxM(0)           <= iethTxM(0);
   iethRxP(0)          <= ethRxP(0);
   iethRxM(0)          <= ethRxM(0);
   iethRxP(3 downto 1) <= (others => '0');
   iethRxM(3 downto 1) <= (others => '0');

   --------------------------------------------------
   -- Application Core Module
   --------------------------------------------------
   U_AppCore : entity work.PgpDpmAppCore
      generic map (
         TPD_G => TPD_C)
      port map (
         -- Debug
         led                => led,
         -- 250 MHz Reference Oscillator 
         locRefClkP         => locRefClkP,
         locRefClkM         => locRefClkM,
         -- RTM High Speed
         dpmToRtmHsP        => dpmToRtmHsP,
         dpmToRtmHsM        => dpmToRtmHsM,
         rtmToDpmHsP        => rtmToDpmHsP,
         rtmToDpmHsM        => rtmToDpmHsM,
         -- DTM Signals
         dtmRefClkP         => dtmRefClkP,
         dtmRefClkM         => dtmRefClkM,
         dtmClkP            => dtmClkP,
         dtmClkM            => dtmClkM,
         dtmFbP             => dtmFbP,
         dtmFbM             => dtmFbM,
         -- CPU System Clocks
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => sysClk200,
         sysClk200Rst       => sysClk200Rst,
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk             => axiClk,
         axiRst             => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         -- DMA Interfaces
         dmaClk             => dmaClk,
         dmaRst             => dmaClkRst,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave);             

end architecture TOP_LEVEL;
