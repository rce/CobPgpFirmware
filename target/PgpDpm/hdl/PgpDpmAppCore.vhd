-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpDpmAppCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2015-07-14
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.AxiLitePkg.all;
use work.RceG3Pkg.all;

library unisim;
use unisim.vcomponents.all;

entity PgpDpmAppCore is
   generic (
      TPD_G : time := 1 ns);
   port (
      -- Debug
      led                : out slv(1 downto 0);
      -- 250 MHz Reference Oscillator 
      locRefClkP         : in  sl;
      locRefClkM         : in  sl;
      -- RTM High Speed
      dpmToRtmHsP        : out slv(11 downto 0);
      dpmToRtmHsM        : out slv(11 downto 0);
      rtmToDpmHsP        : in  slv(11 downto 0);
      rtmToDpmHsM        : in  slv(11 downto 0);
      -- DTM Signals
      dtmRefClkP         : in  sl;
      dtmRefClkM         : in  sl;
      dtmClkP            : in  slv(1 downto 0);
      dtmClkM            : in  slv(1 downto 0);
      dtmFbP             : out sl;
      dtmFbM             : out sl;
      -- CPU System Clocks
      sysClk125          : in  sl;
      sysClk125Rst       : in  sl;
      sysClk200          : in  sl;
      sysClk200Rst       : in  sl;
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk             : in  sl;
      axiRst             : in  sl;
      extAxilReadMaster  : in  AxiLiteReadMasterType;
      extAxilReadSlave   : out AxiLiteReadSlaveType;
      extAxilWriteMaster : in  AxiLiteWriteMasterType;
      extAxilWriteSlave  : out AxiLiteWriteSlaveType;
      -- DMA Interfaces
      dmaClk             : out slv(2 downto 0);
      dmaRst             : out slv(2 downto 0);
      dmaObMaster        : in  AxiStreamMasterArray(2 downto 0);
      dmaObSlave         : out AxiStreamSlaveArray(2 downto 0);
      dmaIbMaster        : out AxiStreamMasterArray(2 downto 0);
      dmaIbSlave         : in  AxiStreamSlaveArray(2 downto 0));         
end PgpDpmAppCore;

architecture mapping of PgpDpmAppCore is

   constant NUM_AXI_MASTERS_C : natural := 12;
   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      0               => (
         baseAddr     => X"A0000000",
         addrBits     => 12,
         connectivity => X"0001"),
      1               => (
         baseAddr     => X"A0010000",
         addrBits     => 12,
         connectivity => X"0001"),
      2               => (
         baseAddr     => X"A0020000",
         addrBits     => 12,
         connectivity => X"0001"),
      3               => (
         baseAddr     => X"A0030000",
         addrBits     => 12,
         connectivity => X"0001"),
      4               => (
         baseAddr     => X"A0040000",
         addrBits     => 12,
         connectivity => X"0001"),
      5               => (
         baseAddr     => X"A0050000",
         addrBits     => 12,
         connectivity => X"0001"),
      6               => (
         baseAddr     => X"A0060000",
         addrBits     => 12,
         connectivity => X"0001"),
      7               => (
         baseAddr     => X"A0070000",
         addrBits     => 12,
         connectivity => X"0001"),
      8               => (
         baseAddr     => X"A0080000",
         addrBits     => 12,
         connectivity => X"0001"),
      9               => (
         baseAddr     => X"A0090000",
         addrBits     => 12,
         connectivity => X"0001"),
      10              => (
         baseAddr     => X"A00A0000",
         addrBits     => 12,
         connectivity => X"0001"),
      11              => (
         baseAddr     => X"A00B0000",
         addrBits     => 12,
         connectivity => X"0001"));          
   signal axiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal axiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   
begin

   --------------------
   -- AXI-Lite Crossbar
   --------------------
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         TPD_G              => TPD_G,
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         DEC_ERROR_RESP_G   => AXI_RESP_OK_C,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         axiClk              => axiClk,
         axiClkRst           => axiRst,
         sAxiWriteMasters(0) => extAxilWriteMaster,
         sAxiWriteSlaves(0)  => extAxilWriteSlave,
         sAxiReadMasters(0)  => extAxilReadMaster,
         sAxiReadSlaves(0)   => extAxilReadSlave,
         mAxiWriteMasters    => axiWriteMasters,
         mAxiWriteSlaves     => axiWriteSlaves,
         mAxiReadMasters     => axiReadMasters,
         mAxiReadSlaves      => axiReadSlaves);     

   -------------------
   -- Application Core
   -------------------
   PgpCore_Inst : entity work.PgpCore
      generic map (
         TPD_G       => TPD_G,
         NUM_LANES_G => NUM_AXI_MASTERS_C)
      port map (
         -- 250 MHz Reference Oscillator 
         locRefClkP      => locRefClkP,
         locRefClkM      => locRefClkM,
         -- High Speed IOs
         gtTxP           => dpmToRtmHsP,
         gtTxN           => dpmToRtmHsM,
         gtRxP           => rtmToDpmHsP,
         gtRxN           => rtmToDpmHsM,
         -- DMA Interfaces
         dmaClk          => dmaClk,
         dmaRst          => dmaRst,
         dmaObMaster     => dmaObMaster,
         dmaObSlave      => dmaObSlave,
         dmaIbMaster     => dmaIbMaster,
         dmaIbSlave      => dmaIbSlave,
         -- AXI-Lite Interface
         axiClk          => axiClk,
         axiRst          => axiRst,
         axiReadMasters  => axiReadMasters,
         axiReadSlaves   => axiReadSlaves,
         axiWriteMasters => axiWriteMasters,
         axiWriteSlaves  => axiWriteSlaves);

   -----------------------
   -- Misc. Configurations
   -----------------------  

   led <= (others => '0');

   GEN_IBUFDS : for i in 0 to 1 generate
      IBUFDS_Inst : IBUFDS
         generic map (
            DIFF_TERM => true) 
         port map(
            I  => dtmClkP(i),
            IB => dtmClkM(i),
            O  => open);
   end generate;

   OBUFDS_Inst : OBUFDS
      port map(
         I  => '0',
         O  => dtmFbP,
         OB => dtmFbM);

end mapping;
