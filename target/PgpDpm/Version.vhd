------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package Version is

   constant FPGA_VERSION_C : std_logic_vector(31 downto 0) := x"DA000002";  -- MAKE_VERSION

   constant BUILD_STAMP_C : string := "PgpDpm: Vivado v2014.4 (x86_64) Built Thu Apr 14 17:27:27 PDT 2016 by wittgen";

end Version;

-------------------------------------------------------------------------------
-- Revision History:
--
-- 07/13/2015 (0xDA000001): Initial Build
-- 03/07/2016 (0xDA000002): Rebuilding with the SVN HEAD
--
-------------------------------------------------------------------------------
