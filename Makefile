
# Default
all: bit

# Clean all firmware builds
clean:
	cd target/PgpDpm/;    make clean
	cd target/PgpDtm/;    make clean

# Build all firmware
bit:
	cd target/PgpDpm/;    make
	cd target/PgpDtm/;    make 

# Synthesis  all firmware (no place and route)
syn:
	cd target/PgpDpm/;    make syn
	cd target/PgpDtm/;    make syn
