-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2016-03-23
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.AxiLitePkg.all;
use work.Pgp2bPkg.all;
use work.RceG3Pkg.all;

library unisim;
use unisim.vcomponents.all;

entity PgpCore is
   generic (
      TPD_G       : time                   := 1 ns;
      NUM_LANES_G : positive range 1 to 12 := 1);
   port (
      -- 250 MHz Reference Oscillator 
      locRefClkP      : in  sl;
      locRefClkM      : in  sl;
      -- High Speed IOs
      gtTxP           : out slv(NUM_LANES_G-1 downto 0);
      gtTxN           : out slv(NUM_LANES_G-1 downto 0);
      gtRxP           : in  slv(NUM_LANES_G-1 downto 0);
      gtRxN           : in  slv(NUM_LANES_G-1 downto 0);
      -- DMA Interfaces
      dmaClk          : out slv(2 downto 0);
      dmaRst          : out slv(2 downto 0);
      dmaObMaster     : in  AxiStreamMasterArray(2 downto 0);
      dmaObSlave      : out AxiStreamSlaveArray(2 downto 0);
      dmaIbMaster     : out AxiStreamMasterArray(2 downto 0);
      dmaIbSlave      : in  AxiStreamSlaveArray(2 downto 0);
      -- AXI-Lite Interface
      axiClk          : in  sl;
      axiRst          : in  sl;
      axiReadMasters  : in  AxiLiteReadMasterArray(NUM_LANES_G-1 downto 0);
      axiReadSlaves   : out AxiLiteReadSlaveArray(NUM_LANES_G-1 downto 0);
      axiWriteMasters : in  AxiLiteWriteMasterArray(NUM_LANES_G-1 downto 0);
      axiWriteSlaves  : out AxiLiteWriteSlaveArray(NUM_LANES_G-1 downto 0));
end PgpCore;

architecture mapping of PgpCore is

   signal gtClkDiv2 : sl;
   signal stableClk : sl;
   signal stableRst : sl;
   signal pgpClk    : sl;
   signal pgpRst    : sl;

   signal pgpTxIn  : Pgp2bTxInArray(NUM_LANES_G-1 downto 0);
   signal pgpTxOut : Pgp2bTxOutArray(NUM_LANES_G-1 downto 0);
   signal pgpRxIn  : Pgp2bRxInArray(NUM_LANES_G-1 downto 0);
   signal pgpRxOut : Pgp2bRxOutArray(NUM_LANES_G-1 downto 0);

   signal pgpTxMasters : AxiStreamMasterVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal pgpTxSlaves  : AxiStreamSlaveVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal pgpRxMasters : AxiStreamMasterVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal pgpRxCtrl    : AxiStreamCtrlVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);

   signal mVcMasters : AxiStreamMasterVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal mVcSlaves  : AxiStreamSlaveVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal sVcMasters : AxiStreamMasterVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);
   signal sVcSlaves  : AxiStreamSlaveVectorArray(NUM_LANES_G-1 downto 0, 3 downto 0);

   signal mLaneMasters : AxiStreamMasterArray(NUM_LANES_G-1 downto 0);
   signal mLaneSlaves  : AxiStreamSlaveArray(NUM_LANES_G-1 downto 0);
   signal sLaneMasters : AxiStreamMasterArray(NUM_LANES_G-1 downto 0);
   signal sLaneSlaves  : AxiStreamSlaveArray(NUM_LANES_G-1 downto 0);
   
begin

   ---------------------------------
   -- Clock and Reset Configurations
   ---------------------------------
   -- GT Reference Clock
   IBUFDS_GTE2_Inst : IBUFDS_GTE2
      port map (
         I     => locRefClkP,           -- 250 MHz
         IB    => locRefClkM,
         CEB   => '0',
         ODIV2 => gtClkDiv2,
         O     => open);

   BUFG_Inst : BUFG
      port map (
         I => gtClkDiv2,
         O => stableClk);               -- 125 MHz

   PwrUpRst_Inst : entity work.PwrUpRst
      generic map(
         DURATION_G => getTimeRatio(125.0E6, 1.0E+3))  -- 1 ms power up reset
      port map (
         clk    => stableClk,
         rstOut => stableRst);   

   U_ClkManager : entity work.ClockManager7
      generic map(
         TPD_G              => TPD_G,
         TYPE_G             => "MMCM",
         INPUT_BUFG_G       => false,
         FB_BUFG_G          => false,
         RST_IN_POLARITY_G  => '1',
         NUM_CLOCKS_G       => 1,
         -- MMCM attributes
         BANDWIDTH_G        => "OPTIMIZED",
         CLKIN_PERIOD_G     => 8.0,     -- 125 MHz
         DIVCLK_DIVIDE_G    => 4,
         CLKFBOUT_MULT_F_G  => 31.875,     -- 1 GHz = 8 x 125 MHz
         CLKOUT0_DIVIDE_F_G => 6.375)     -- 156.25 MHz = 1 GHz/6.4
      port map(
         -- Clock Input
         clkIn     => stableClk,
         rstIn     => stableRst,
         -- Clock Outputs
         clkOut(0) => pgpClk,
         -- Reset Outputs
         rstOut(0) => pgpRst); 

   -- Set the DMA Clocks and Resets
   dmaClk <= (others => pgpClk);
   dmaRst <= (others => pgpRst);

   ---------------------
   -- DMA Configurations
   ---------------------

   -- DMA[2] = Loopback Configuration
   dmaIbMaster(2) <= dmaObMaster(2);
   dmaObSlave(2)  <= dmaIbSlave(2);

   -- DMA[1] = Loopback Configuration
   dmaIbMaster(1) <= dmaObMaster(1);
   dmaObSlave(1)  <= dmaIbSlave(1);

   -- DMA[0] = 3.125 Gbps PGP Interface
   GEN_PGP :
   for lane in NUM_LANES_G-1 downto 0 generate
      
      Pgp2bGtx7VarLat_Inst : entity work.Pgp2bGtx7VarLat
         generic map (
            TPD_G             => TPD_G,
            -- CPLL Configurations
            TX_PLL_G          => "CPLL",
            RX_PLL_G          => "CPLL",
            CPLL_REFCLK_SEL_G => "111",
            CPLL_FBDIV_G      => 4,
            CPLL_FBDIV_45_G   => 5,
            CPLL_REFCLK_DIV_G => 1,
            -- MGT Configurations
            RXOUT_DIV_G       => 2,
            TXOUT_DIV_G       => 2,
            RX_CLK25_DIV_G    => 7,
            TX_CLK25_DIV_G    => 7,
            RX_OS_CFG_G       => "0000010000000",
            RXCDR_CFG_G       => x"03000023ff40200020",
            RXDFEXYDEN_G      => '1',
            RX_DFE_KL_CFG2_G  => X"301148AC",
            -- VC Configuration
            NUM_VC_EN_G       => 4)          
         port map (
            -- GT Clocking
            stableClk        => stableClk,
            gtCPllRefClk     => pgpClk,
            gtCPllLock       => open,
            gtQPllRefClk     => '0',
            gtQPllClk        => '0',
            gtQPllLock       => '1',
            gtQPllRefClkLost => '0',
            gtQPllReset      => open,
            -- GT Serial IO
            gtTxP            => gtTxP(lane),
            gtTxN            => gtTxN(lane),
            gtRxP            => gtRxP(lane),
            gtRxN            => gtRxN(lane),
            -- Tx Clocking
            pgpTxReset       => pgpRst,
            pgpTxRecClk      => open,
            pgpTxClk         => pgpClk,
            pgpTxMmcmReset   => open,
            pgpTxMmcmLocked  => '1',
            -- Rx clocking
            pgpRxReset       => pgpRst,
            pgpRxRecClk      => open,
            pgpRxClk         => pgpClk,
            pgpRxMmcmReset   => open,
            pgpRxMmcmLocked  => '1',
            -- Non VC Rx Signals
            pgpRxIn          => pgpRxIn(lane),
            pgpRxOut         => pgpRxOut(lane),
            -- Non VC Tx Signals
            pgpTxIn          => pgpTxIn(lane),
            pgpTxOut         => pgpTxOut(lane),
            -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
            pgpTxMasters(3)  => pgpTxMasters(lane, 3),
            pgpTxMasters(2)  => pgpTxMasters(lane, 2),
            pgpTxMasters(1)  => pgpTxMasters(lane, 1),
            pgpTxMasters(0)  => pgpTxMasters(lane, 0),
            pgpTxSlaves(3)   => pgpTxSlaves(lane, 3),
            pgpTxSlaves(2)   => pgpTxSlaves(lane, 2),
            pgpTxSlaves(1)   => pgpTxSlaves(lane, 1),
            pgpTxSlaves(0)   => pgpTxSlaves(lane, 0),
            -- Frame Receive Interface - 1 Lane, Array of 4 VCs
            pgpRxMasters(3)  => pgpRxMasters(lane, 3),
            pgpRxMasters(2)  => pgpRxMasters(lane, 2),
            pgpRxMasters(1)  => pgpRxMasters(lane, 1),
            pgpRxMasters(0)  => pgpRxMasters(lane, 0),
            pgpRxCtrl(3)     => pgpRxCtrl(lane, 3),
            pgpRxCtrl(2)     => pgpRxCtrl(lane, 2),
            pgpRxCtrl(1)     => pgpRxCtrl(lane, 1),
            pgpRxCtrl(0)     => pgpRxCtrl(lane, 0));

      ----------------------------------
      -- AXI-Lite: PGP Monitoring Module
      ----------------------------------
      Pgp2bAxi_Inst : entity work.Pgp2bAxi
         generic map (
            TPD_G              => TPD_G,
            COMMON_TX_CLK_G    => false,
            COMMON_RX_CLK_G    => false,
            WRITE_EN_G         => true,
            AXI_CLK_FREQ_G     => 125.0E+6,
            STATUS_CNT_WIDTH_G => 32,
            ERROR_CNT_WIDTH_G  => 4)
         port map (
            -- TX PGP Interface (pgpTxClk)
            pgpTxClk        => pgpClk,
            pgpTxClkRst     => pgpRst,
            pgpTxIn         => pgpTxIn(lane),
            pgpTxOut        => pgpTxOut(lane),
            -- RX PGP Interface (pgpRxClk)
            pgpRxClk        => pgpClk,
            pgpRxClkRst     => pgpRst,
            pgpRxIn         => pgpRxIn(lane),
            pgpRxOut        => pgpRxOut(lane),
            -- AXI-Lite Register Interface (axilClk domain)
            axilClk         => axiClk,
            axilRst         => axiRst,
            axilReadMaster  => axiReadMasters(lane),
            axilReadSlave   => axiReadSlaves(lane),
            axilWriteMaster => axiWriteMasters(lane),
            axilWriteSlave  => axiWriteSlaves(lane));              

      GEN_VC :
      for vc in 3 downto 0 generate
         
         VCRX_FIFO : entity work.AxiStreamFifo
            generic map (
               -- General Configurations
               TPD_G               => TPD_G,
               PIPE_STAGES_G       => 0,
               SLAVE_READY_EN_G    => false,
               VALID_THOLD_G       => 1,
               -- FIFO configurations
               BRAM_EN_G           => true,
               XIL_DEVICE_G        => "7SERIES",
               USE_BUILT_IN_G      => false,
               GEN_SYNC_FIFO_G     => true,
               CASCADE_SIZE_G      => 1,
               FIFO_ADDR_WIDTH_G   => 9,
               FIFO_FIXED_THRESH_G => true,
               FIFO_PAUSE_THRESH_G => 128,
               -- AXI Stream Port Configurations
               SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
               MASTER_AXI_CONFIG_G => RCEG3_AXIS_DMA_CONFIG_C)            
            port map (
               -- Slave Port
               sAxisClk    => pgpClk,
               sAxisRst    => pgpRst,
               sAxisMaster => pgpRxMasters(lane, vc),
               sAxisCtrl   => pgpRxCtrl(lane, vc),
               -- Master Port
               mAxisClk    => pgpClk,
               mAxisRst    => pgpRst,
               mAxisMaster => mVcMasters(lane, vc),
               mAxisSlave  => mVcSlaves(lane, vc)); 

         VCTX_FIFO : entity work.AxiStreamFifo
            generic map (
               -- General Configurations
               TPD_G               => TPD_G,
               PIPE_STAGES_G       => 0,
               SLAVE_READY_EN_G    => true,
               VALID_THOLD_G       => 1,
               -- FIFO configurations
               BRAM_EN_G           => true,
               XIL_DEVICE_G        => "7SERIES",
               USE_BUILT_IN_G      => false,
               GEN_SYNC_FIFO_G     => true,
               CASCADE_SIZE_G      => 1,
               FIFO_ADDR_WIDTH_G   => 9,
               FIFO_FIXED_THRESH_G => true,
               FIFO_PAUSE_THRESH_G => 128,
               -- AXI Stream Port Configurations
               SLAVE_AXI_CONFIG_G  => RCEG3_AXIS_DMA_CONFIG_C,
               MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)    
            port map (
               -- Slave Port
               sAxisClk    => pgpClk,
               sAxisRst    => pgpRst,
               sAxisMaster => sVcMasters(lane, vc),
               sAxisSlave  => sVcSlaves(lane, vc),
               -- Master Port
               mAxisClk    => pgpClk,
               mAxisRst    => pgpRst,
               mAxisMaster => pgpTxMasters(lane, vc),
               mAxisSlave  => pgpTxSlaves(lane, vc));

      end generate GEN_VC;

      ------------------------
      -- AXIS MUX for VC buses
      ------------------------
      AxiStreamMux_Inst : entity work.AxiStreamMux
         generic map (
            TPD_G         => TPD_G,
            NUM_SLAVES_G  => 4,
            TDEST_HIGH_G  => 3,
            TDEST_LOW_G   => 0,
            PIPE_STAGES_G => 2)
         port map (
            -- Clock and reset
            axisClk         => pgpClk,
            axisRst         => pgpRst,
            -- Slaves
            sAxisMasters(3) => mVcMasters(lane, 3),
            sAxisMasters(2) => mVcMasters(lane, 2),
            sAxisMasters(1) => mVcMasters(lane, 1),
            sAxisMasters(0) => mVcMasters(lane, 0),
            sAxisSlaves(3)  => mVcSlaves(lane, 3),
            sAxisSlaves(2)  => mVcSlaves(lane, 2),
            sAxisSlaves(1)  => mVcSlaves(lane, 1),
            sAxisSlaves(0)  => mVcSlaves(lane, 0),
            -- Master
            mAxisMaster     => mLaneMasters(lane),
            mAxisSlave      => mLaneSlaves(lane));

      --------------------------
      -- AXIS DEMUX for VC buses
      --------------------------
      AxiStreamDeMux_Inst : entity work.AxiStreamDeMux
         generic map (
            TPD_G         => TPD_G,
            NUM_MASTERS_G => 4,
            TDEST_HIGH_G  => 3,
            TDEST_LOW_G   => 0)         
         port map (
            -- Clock and reset
            axisClk         => pgpClk,
            axisRst         => pgpRst,
            -- Slaves
            sAxisMaster     => sLaneMasters(lane),
            sAxisSlave      => sLaneSlaves(lane),
            -- Master
            mAxisMasters(3) => sVcMasters(lane, 3),
            mAxisMasters(2) => sVcMasters(lane, 2),
            mAxisMasters(1) => sVcMasters(lane, 1),
            mAxisMasters(0) => sVcMasters(lane, 0),
            mAxisSlaves(3)  => sVcSlaves(lane, 3),
            mAxisSlaves(2)  => sVcSlaves(lane, 2),
            mAxisSlaves(1)  => sVcSlaves(lane, 1),
            mAxisSlaves(0)  => sVcSlaves(lane, 0));           

   end generate GEN_PGP;

   --------------------------
   -- AXIS MUX for Lane buses
   --------------------------
   AxiStreamMux_Inst : entity work.AxiStreamMux
      generic map (
         TPD_G         => TPD_G,
         NUM_SLAVES_G  => NUM_LANES_G,
         TDEST_HIGH_G  => 7,
         TDEST_LOW_G   => 4,
         PIPE_STAGES_G => 2)
      port map (
         -- Clock and reset
         axisClk      => pgpClk,
         axisRst      => pgpRst,
         -- Slaves
         sAxisMasters => mLaneMasters,
         sAxisSlaves  => mLaneSlaves,
         -- Master
         mAxisMaster  => dmaIbMaster(0),
         mAxisSlave   => dmaIbSlave(0));

   ----------------------------
   -- AXIS DEMUX for Lane buses
   ----------------------------
   AxiStreamDeMux_Inst : entity work.AxiStreamDeMux
      generic map (
         TPD_G         => TPD_G,
         NUM_MASTERS_G => NUM_LANES_G,
         TDEST_HIGH_G  => 7,
         TDEST_LOW_G   => 4)         
      port map (
         -- Clock and reset
         axisClk      => pgpClk,
         axisRst      => pgpRst,
         -- Slaves
         sAxisMaster  => dmaObMaster(0),
         sAxisSlave   => dmaObSlave(0),
         -- Master
         mAxisMasters => sLaneMasters,
         mAxisSlaves  => sLaneSlaves);  

end mapping;
